#!/bin/bash
sudo cp libs/macosx-gcc/ikpMP3.dylib /usr/local/lib
sudo cp libs/macosx-gcc/libirrklang.dylib /usr/local/lib
qmake
make
mkdir wavelow.app/Contents
mkdir wavelow.app/Contents/Frameworks
cp -R libs/macosx-gcc/ wavelow.app/Contents/Frameworks/
cp -R libs/macosx-gcc/ wavelow.app/Contents/MacOs/
cp -R /usr/local/opt/qt@5/lib/QtWidgets.framework wavelow.app/Contents/Frameworks
cp -R /usr/local/opt/qt@5/lib/QtCore.framework wavelow.app/Contents/Frameworks
cp -R /usr/local/opt/qt@5/lib/QtGui.framework wavelow.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore wavelow.app/Contents/Frameworks/QtCore.framework/Versions/5/QtCore
install_name_tool -id @executable_path/../Frameworks/QtGui.framework/Versions/5/QtGui wavelow.app/Contents/Frameworks/QtGui.framework/Versions/5/QtGui
install_name_tool -id @executable_path/../Frameworks/QtWidgets.framework/Versions/5/QtWidgets wavelow.app/Contents/Frameworks/QtWidgets.framework/Versions/5/QtWidgets
install_name_tool -change /usr/local/opt/qt@5/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore wavelow.app/Contents/MacOs/wavelow
install_name_tool -change /usr/local/opt/qt@5/lib/QtGui.framework/Versions/5/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/5/QtGui wavelow.app/Contents/MacOs/wavelow
install_name_tool -change /usr/local/opt/qt@5/lib/QtWidgets.framework/Versions/5/QtWidgets @executable_path/../Frameworks/QtWidgets.framework/Versions/5/QtWidgets wavelow.app/Contents/MacOs/wavelow
install_name_tool -change /usr/local/opt/qt@5/lib/QtCore.framework/Versions/5/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/5/QtCore wavelow.app/Contents/Frameworks/QtGui.framework/Versions/5/QtGui
touch wavelow.app/Contents/MacOS/playlists.txt
